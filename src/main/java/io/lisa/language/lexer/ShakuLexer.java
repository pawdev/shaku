package io.lisa.language.lexer;

import io.lisa.language.lexer.position.Position;
import io.lisa.language.lexer.position.TextSection;
import io.lisa.language.lexer.token.LexicalToken;
import io.lisa.language.lexer.token.Token;

import java.util.ArrayList;
import java.util.List;

public class ShakuLexer {

    public static final LexicalContext<String> lexicalAnalysesContext =
            new LexicalContext<String>()
                    .addRule("white_space", rule -> rule
                            .addMultiline("/*", "*/")
                            .addRegex("//[^\\r\\n]*")
                            .addRegex("[ \t\r\n]+"))

                    .addRule("equals", rule -> rule.addString("=="))
                    .addRule("!equals", rule -> rule.addString("!="))

                    .addRule("less-equals", rule -> rule.addStrings("<=", "=<"))
                    .addRule("more-equals", rule -> rule.addStrings(">=", "=>"))
                    .addRule("bit-less-equals", rule -> rule.addStrings("<<=", "=<<"))
                    .addRule("bit-more-equals", rule -> rule.addStrings(">>=", "=>>"))

                    .addRule("less", rule -> rule.addString("<"))
                    .addRule("more", rule -> rule.addString(">"))

                    .addRule("and", rule -> rule.addString("&&"))
                    .addRule("or", rule -> rule.addString("||"))

                    .addRule("negate", rule -> rule.addString("!"))
                    .addRule("question", rule -> rule.addString("?"))

                    .addRule("nor", rule -> rule.addString("~"))
                    .addRule("xor", rule -> rule.addString("^"))

                    .addRule("x-and", rule -> rule.addString("&"))
                    .addRule("x-or", rule -> rule.addString("|"))

                    .addRule("increase", rule -> rule.addString("++"))
                    .addRule("decrease", rule -> rule.addString("--"))

                    .addRule("min", rule -> rule.addString("-"))
                    .addRule("min-equals", rule -> rule.addString("-="))

                    .addRule("plus", rule -> rule.addString("+"))
                    .addRule("plus-equals", rule -> rule.addString("+="))

                    .addRule("mul", rule -> rule.addString("*"))
                    .addRule("mul-equals", rule -> rule.addString("*="))

                    .addRule("dif", rule -> rule.addString("/"))
                    .addRule("dif-equals", rule -> rule.addString("/="))

                    .addRule("mod", rule -> rule.addString("%"))
                    .addRule("mod-equals", rule -> rule.addString("%="))

                    .addRule("shift-left", rule -> rule.addString("<<"))
                    .addRule("shift-right", rule -> rule.addString(">>"))

                    .addRule("identifier", rule -> rule.addRegex("[a-zA-Z_][a-zA-Z0-9_]*"))

                    .addRule("question-equals", rule -> rule.addString("?="))
                    .addRule("equals", rule -> rule.addString("="))

                    .addRule("array", rule -> rule.addString("..."))
                    .addRule("dot-dot", rule -> rule.addString(".."))
                    .addRule("dot", rule -> rule.addString("."))

                    .addRule("semicolon", rule -> rule.addString(";"))
                    .addRule("namespace", rule -> rule.addString("::"))
                    .addRule("colon", rule -> rule.addString(":"))

                    .addRule("comma", rule -> rule.addString(","))

                    .addRule("bool", rule -> rule.addStrings("true", "false"))
                    .addRule("string", rule -> rule.addMultiline("\"", "\\", "\""))
                    .addRule("character", rule -> rule.addMultiline("'", "\\", "'"))

                    .addRule("floating-number", rule -> rule.addRegex("[0-9]+.[0-9]+"))
                    .addRule("true-number", rule -> rule.addRegex("[0-9]+"))
                    .addRule("hex-number", rule -> rule.addRegex("0x[0-9a-fA-F]+"))

                    .addRule("left-paren", rule -> rule.addString("("))
                    .addRule("right-paren", rule -> rule.addString(")"))
                    .addRule("left-square", rule -> rule.addString("["))
                    .addRule("right-square", rule -> rule.addString("]"))
                    .addRule("left-curly", rule -> rule.addString("{"))
                    .addRule("right-curly", rule -> rule.addString("}"))

                    // keywords
                    .addRule("bool", rule -> rule.addStrings("bool", "boolean"))
                    .addRule("string", rule -> rule.addString("string"))

                    // floating
                    .addRule("f32", rule -> rule.addStrings("f32", "float"))
                    .addRule("f64", rule -> rule.addStrings("f64", "double"))
                    .addRule("str", rule -> rule.addStrings("str", "char"))
                    // signed
                    .addRule("i8", rule -> rule.addStrings("i8", "byte"))
                    .addRule("i16", rule -> rule.addStrings("i16", "short"))
                    .addRule("i32", rule -> rule.addStrings("i32", "int"))
                    .addRule("i64", rule -> rule.addStrings("i64", "long"))
                    .addRule("i128", rule -> rule.addStrings("i128"))
                    // unsigned
                    .addRule("u8", rule -> rule.addString("u8"))
                    .addRule("u16", rule -> rule.addString("u16"))
                    .addRule("u32", rule -> rule.addString("u32"))
                    .addRule("u64", rule -> rule.addString("u64"))
                    .addRule("u128", rule -> rule.addString("u128"))

                    .addRule("function", rule -> rule.addStrings("fn", "function"))
                    .addRule("object", rule -> rule.addString("object"))
                    .addRule("record", rule -> rule.addString("record"))
                    .addRule("use", rule -> rule.addString("use"))
                    .addRule("let", rule -> rule.addString("let"))
                    .addRule("from", rule -> rule.addString("from"))
                    .addRule("if", rule -> rule.addString("if"))
                    .addRule("else", rule -> rule.addString("else"))
                    .addRule("while", rule -> rule.addString("while"))
                    .addRule("for", rule -> rule.addString("for"))
                    .addRule("switch", rule -> rule.addString("switch"))
                    .addRule("match", rule -> rule.addString("match"))
                    .addRule("continue", rule -> rule.addString("continue"))
                    .addRule("case", rule -> rule.addString("case"))
                    .addRule("break", rule -> rule.addString("break"))
                    .addRule("compiler", rule -> rule.addString("compiler"))
                    .addRule("vec", rule -> rule.addStrings("vec", "vector"))

                    .addRule("access-public", rule -> rule.addStrings("pub", "public"))
                    .addRule("access-global", rule -> rule.addString("global"))
                    .addRule("access-volatile", rule -> rule.addString("volatile"))
                    .addRule("access-constant", rule -> rule.addStrings("constant", "const"))
                    .addRule("access-static", rule -> rule.addStrings("static"))

                    .toImmutable();

    public List<Token> lex(final String content, final boolean keepWhitespace) {
        List<Token> tokenList = new ArrayList<>();
        int offset = 0;
        int line = 0;
        int column = 0;
        int length = content.length();

        String input = content;

        while (offset < length) {
            Position startPos = new Position(column, line); // offset

            LexicalToken<String> lexerToken = lexicalAnalysesContext.nextToken(input);
            if (lexerToken == null) {
                throw new RuntimeException("Could not parse token at position: " + startPos);
            }

            if (lexerToken.getLength() + offset > length) {
                break;
            }

            for (int i = offset; i < offset + lexerToken.getLength(); i++) {
                char c = content.charAt(i);

                if (c == '\n') {
                    line++;
                    column = 0;
                } else {
                    column += (c == '\t') ? 4 : 1;
                }
            }

            Position endPos = new Position(column, line); // offset + lexerToken.length
            tokenList.add(new Token(
                    lexerToken.getContent(),
                    lexerToken.getType(),
                    new TextSection(startPos, endPos)
            ));

            input = input.substring(lexerToken.getLength());
            offset += lexerToken.getLength();
        }

        if (keepWhitespace)
            return tokenList;

        tokenList.removeIf(token -> token.type().equals("white_space"));
        return tokenList;
    }

}
