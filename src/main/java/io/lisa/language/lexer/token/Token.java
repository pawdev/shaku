package io.lisa.language.lexer.token;

import io.lisa.language.lexer.position.TextSection;

/**
 * The type Token.
 */
public record Token(String value, String type, TextSection position) {


    /**
     * Gets value.
     *
     * @return the value
     */
    @Override
    public String value() {
        return value;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    @Override
    public String type() {
        return type;
    }
}
