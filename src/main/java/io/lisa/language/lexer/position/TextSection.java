package io.lisa.language.lexer.position;

public record TextSection(Position start, Position end) { }
