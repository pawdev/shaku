package io.lisa.language.lexer.position;

public record Position(int colon, int line) {
    @Override
    public String toString() {
        return "(" +
                "colon=" + colon +
                ", line=" + line +
                ')';
    }
}
