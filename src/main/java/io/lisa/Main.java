package io.lisa;

import io.lisa.language.lexer.ShakuLexer;
import io.lisa.language.lexer.token.Token;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final File EXAMPLE_FILE = Paths.get("./example/main.sha").toFile();
    private static final ShakuLexer SHAKU_LEXER = new ShakuLexer();

    public static void main(String[] args) throws FileNotFoundException {
        final StringBuilder content = new StringBuilder();
        final Scanner scanner = new Scanner(EXAMPLE_FILE);
        while (scanner.hasNextLine())
            content.append(scanner.nextLine()).append("\n");
        scanner.close();

        final String source = content.toString();

        final List<Token> tokens = SHAKU_LEXER.lex(source, false);
        tokens.forEach(System.out::println);
    }
}